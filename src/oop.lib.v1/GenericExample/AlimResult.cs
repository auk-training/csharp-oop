﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

namespace oop.lib.v1.GenericExample
{
    public class AlimResult<T>
    {
        public AlimResult(T result)
        {
            Result = result;
        }

        public T Result { get; }

        public string Status { get; set; }

        public int ErrorCode { get; set; }
    }

    // public class AlimAdapter<T, T2>
    // {
    //     public T2 Convert(T itemIn)
    //     {
    //         
    //     }
    // }
}
