﻿namespace oop.lib.v1.GenericExample
{
    public static class AlimStatic
    {
        public static void DoSomething<T>(T arg)
        { }

        public static T2 DoSomething2<T, T2>(T arg) where T: class, new() where T2 : new()
        {
            var x = new T();

            return default;
        }
    }
}