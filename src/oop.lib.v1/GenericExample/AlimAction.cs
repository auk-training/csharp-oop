﻿using System;

namespace oop.lib.v1.GenericExample
{
    public class AlimAction<T>
    {
        public void DoWork(Action<T> doer, T param)
        {
            doer(param);
        }
    }
}
