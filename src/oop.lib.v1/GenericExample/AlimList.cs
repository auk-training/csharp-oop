﻿using System;

namespace oop.lib.v1.GenericExample
{
    public class AlimList<T>
    {
        public AlimList(int capacity = 5) => MyArray = new T[capacity];

        private int Position { get; set; }
        private T[] MyArray { get; }

        public int Length => MyArray.Length;

        public void Add(T item)
        {
            MyArray[Position++] = item;
        }

        public void Print()
        {
            if (Position <= 0)
            {
                return;
            }

            Console.WriteLine();

            for (var i = 0; i < Position; i++)
            {
                Console.Write($"{i} : [{MyArray[i]}] ");
            }

            Console.WriteLine();
        }
    }
}