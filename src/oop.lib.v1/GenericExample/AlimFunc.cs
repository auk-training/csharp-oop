﻿using System;

namespace oop.lib.v1.GenericExample
{
    public class AlimFunc<T>
    {
        public void DoWork<T1, TResult>(Func<T,T1, TResult> doer, T arg, T1 arg2)
        {
            var result = doer(arg, arg2);
        }
    }
}
