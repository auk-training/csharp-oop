﻿using System;
using System.Collections.Generic;

namespace oop.lib.v1
{
    public class Person
    {
        // Field
        public string FirstName { get; set; }

        public string LastName { get; set; }

        private IList<string> _namesList = new List<string>(10);

        /**
         * Private Constructors
         * 1. Reflection create dynamically (high)
         * 2. Restrict Developers to directly create this class.
         * 3. Multiple constructors
         *      - One or two - ~ can be private, I want to do something inside the class only 
         */

        /**
        * Multiple Constructors
        * 1. One single you must call the new for that constructor only.
        * 2. You can overloading
        * 3. You can call default constructor without having any constructors
          *      - However, if you have one you have to call using that.
          *      - To have the default constructor you must create the default one again.
        */

        public Person()
        {
        }

        ~Person()
        {
            _namesList?.Clear();
            Console.WriteLine($"Garbage collect - Person class - {FirstName}");
        }
    }
}
