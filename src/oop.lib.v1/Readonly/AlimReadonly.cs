﻿using System.Security.Principal;

namespace oop.lib.v1.Readonly
{
    public class AlimReadonly
    {
        private readonly string _name;
        private const string _nameX = "A";
        private static readonly string _staticReadonlyName;

        public AlimReadonly(string name)
        {
            _name               = name;
            _name               = name + "hello";
        }

        static AlimReadonly()
        {
            _staticReadonlyName = "";
            _staticReadonlyName = "";
        }

        public  void DoSomething()
        {
            // Name = "";
            // _staticReadonlyName = "";
            var x = _name;
        }
    }
}
