﻿using oop.lib.v1.StaticModifier;

namespace oop.lib.v1
{
    class Person2
    {
        // Field
        public string FirstName;

        private Person2(string firstName) => FirstName = firstName;

        public void SampleFunction()
        {
            var p = new Person();
            // StaticAlim.DoSomething();
        }
    }
}
