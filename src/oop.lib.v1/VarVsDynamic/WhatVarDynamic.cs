﻿namespace oop.lib.v1.VarVsDynamic
{
    public class WhatVarDynamic
    {
        public void VarMeans()
        {
            var x     = "Md. Alim Ul Karim";
            var email = "alim.karim@the-xproduct.com";

            string x2 = "ddd";

            // x2 = 1;
            // x  = 134214;
            float x5 = 5 / (float)2 ;
            var   x6 = 5 / (float)2;


        }

        public void DynamicMeans()
        {
            dynamic x     = "Md. Alim Ul Karim";
            var email = "alim.karim@the-xproduct.com";

            string x2 = "ddd";

            // x2 = 1;
            x  = 134214;
        }
    }
}
