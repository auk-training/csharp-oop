﻿namespace oop.lib.v1.RefOutTest
{
    public class RefOutTesting
    {
        public void DoWork()
        {
            int    x;
            string y = "Alim Ul Karim";

            // DealWithRef( ref x,5, ref y);
            DealWithOut(out x, 5, out y);
        }

        public string DealWithRef( ref int x,
                                   int m, ref string y)
        {
            return "some changes";
        }

        public string DealWithOut(
            out int x,
            int m,
            out string y)
        {
            x = -5;
            y = "some out value";


            return "out variable";
        }
    }
}
