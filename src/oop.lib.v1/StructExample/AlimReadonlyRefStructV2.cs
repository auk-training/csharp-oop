﻿using System;

namespace oop.lib.v1.StructExample
{
    public readonly ref struct AlimReadonlyRefStructV2
    {
        public AlimReadonlyRefStructV2(
            string name,
            int someInt,
            DateTime? someDate)
        {
            Name     = name;
            SomeInt  = someInt;
            SomeDate = someDate;
        }

        public string Name { get; }

        public int SomeInt { get;  }

        public DateTime? SomeDate { get;  }
    }
}