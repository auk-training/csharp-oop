﻿using System;

namespace oop.lib.v1.StructExample
{
    public struct AlimStructV1
    {
        public void StructCreationTest()
        {
            int? x = null;

            Console.WriteLine($"value of x direct : {x}");

            // Console.WriteLine($"value of x.value : {x.Value}");
            Console.WriteLine($"value of x.HasValue : {x.HasValue}");

            // if (x.HasValue)
            // {
            //     x.Value
            // }
            // https://t.ly/RqFXX
            void localFunc1(string line)
            {
                var toLocal = new AlimRefStructV1();
            }

            void localFunc2(AlimRefStructV1 line2)
            {
                var toLocal = new AlimRefStructV1();
            }

            var x2 = new AlimRefStructV1();
            x2      = new AlimRefStructV1();
            x2.Name = "";

            var r1 = new AlimReadonlyRefStructV2("", 1, new DateTime());
            r1 = new AlimReadonlyRefStructV2();

            //var m2 = new Alim
        }
    }
}
