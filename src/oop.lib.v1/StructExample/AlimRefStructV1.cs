﻿using System;

namespace oop.lib.v1.StructExample
{
    public ref struct AlimRefStructV1
    {
        public string Name { get; set; }

        public int SomeInt { get; set; }

        public DateTime? SomeDate { get; set; }
    }
}