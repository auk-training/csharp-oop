﻿using System;

namespace oop.lib.v1.StaticModifier
{
    public class NonStaticOop
    {
        private static string _staticName;

        public static string StaticName
        {
            get
            {
                Console.WriteLine("StaticName getter is called");

                return _staticName;
            }
            set
            {
                Console.WriteLine("StaticName SET is called");

                _staticName = value;
            }
        }

        public string Name { get; set; }

        private void DoSomething()
        {
        }
    }
}
