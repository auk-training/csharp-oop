﻿using System;

namespace oop.lib.v1.StaticModifier
{
    public static class StaticAlim
    {
        public static readonly string SomeField = "static field";

        static StaticAlim()
        {
            Console.WriteLine("static Alim - Static Constructor");
        }

        public static void DoSomething()
        {
            Console.WriteLine("static Alim - Do Something!");
        }
    }
}
