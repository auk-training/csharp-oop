﻿// See https://aka.ms/new-console-template for more information

using oop.lib.v1;
using oop.lib.v1.GenericExample;
using oop.lib.v1.StaticModifier;
using oop.lib.v1.StructExample;

// var x = new NonStaticOop();
//
// x.Name                  = "Alim 1";
// NonStaticOop.StaticName = "static name";
//
// var y = new NonStaticOop();
// y.Name                  = "Alim 2";
// Console.WriteLine(x.Name);
// Console.WriteLine(y.Name);
// Console.WriteLine(NonStaticOop.StaticName);

//
// var alimList = new AlimList<string>(30);
//
// alimList.Add("alim created");
// alimList.Add("a generic");
// alimList.Add("list");
// alimList.Print();
//
//
// var alimInt = new AlimList<int>(30);
//
// alimInt.Add(1);
// alimInt.Add(5);
// alimInt.Add(8);
// alimInt.Print();
// Console.WriteLine(StaticAlim.SomeField);
// StaticAlim.DoSomething();
// StaticAlim.DoSomething();
// StaticAlim.DoSomething();
// StaticAlim.DoSomething();

var x = new AlimStructV1();
x.StructCreationTest();
