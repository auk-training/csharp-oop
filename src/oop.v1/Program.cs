﻿using System;
using oop.lib.v1;

namespace oop.v1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person alimPerson = new Person();

            alimPerson.FirstName = "alim";
            alimPerson           = null;
            GC.Collect();

            Console.WriteLine("Hello World");
        }
    }
}
