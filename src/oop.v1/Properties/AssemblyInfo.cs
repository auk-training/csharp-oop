﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("oop.v1")]
[assembly: AssemblyDescription("Training Project By Md. Alim Ul Karim for OOP to OOD, 2023")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("alimkarim.com")]
[assembly: AssemblyProduct("oop.v1")]
[assembly: AssemblyCopyright("Copyright by Md. Alim Ul Karim ©  2023")]
[assembly: AssemblyTrademark("alimkarim.com")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("18A4E3CA-F0E9-410D-947D-6A2017930A92")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
